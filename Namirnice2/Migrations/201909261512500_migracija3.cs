namespace Namirnice2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracija3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FileUploadModels", "Naziv", c => c.String(nullable: false));
            AlterColumn("dbo.FileUploadModels", "Cena", c => c.Single(nullable: false));
            AlterColumn("dbo.FileUploadModels", "Opis", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FileUploadModels", "Opis", c => c.String());
            AlterColumn("dbo.FileUploadModels", "Cena", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.FileUploadModels", "Naziv", c => c.String());
        }
    }
}
