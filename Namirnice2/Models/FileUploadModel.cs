﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Namirnice2.Models
{
    public class FileUploadModel
    {
        public int Id { get; set; }
        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string fileUploaded { get; set; } //putanja sacuvana
        [Required]
        public string Naziv { get; set; }
        [Required]
        [Range(0, float.MaxValue)]
        public float Cena { get; set; }
        [Required]
        public string Opis { get; set; }
        [Required]
        public decimal ZahtevanaKolicina { get; set; }
        [Required]
        public decimal TrenutnaKolicina { get; set; }

        private decimal razlika;
        public decimal Razlika
        { get => razlika=TrenutnaKolicina-ZahtevanaKolicina;
            set=>  razlika=value;
        }
        
    }
}
