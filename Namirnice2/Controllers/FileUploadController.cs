﻿using Namirnice2.Models;
using Namirnice2.Repository;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Namirnice2.Controllers
{
    public class FileUploadController : Controller
    {

        private IUploadFile fileUploadRepo = new FileUploadRepo();

        // GET: FileUpload
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Naziv_desc" : "";
            ViewBag.TrenutnaKolicinaSortParm = sortOrder == "Trenutna_Kolicina" ? "Trenutna_Kolicina_desc" : "Trenutna_Kolicina";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var uploadovaniFajlovi = from s in fileUploadRepo.GetAll()
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                uploadovaniFajlovi = uploadovaniFajlovi.Where(s => s.Naziv.Contains(searchString)
                                       || s.Opis.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "Naziv_desc":
                    uploadovaniFajlovi = uploadovaniFajlovi.OrderByDescending(s => s.Naziv);
                    break;
                case "Trenutna_Kolicina":
                    uploadovaniFajlovi = uploadovaniFajlovi.OrderBy(s => s.TrenutnaKolicina);
                    break;
                case "Trenutna_Kolicina_desc":
                    uploadovaniFajlovi = uploadovaniFajlovi.OrderByDescending(s => s.TrenutnaKolicina);
                    break;
                default:  // Name ascending 
                    uploadovaniFajlovi = uploadovaniFajlovi.OrderBy(s => s.Naziv);
                    break;
            }

            int pageSize = 7;
            int pageNumber = (page ?? 1);
            return View(uploadovaniFajlovi.ToPagedList(pageNumber, pageSize));

            //var sviFajlovi = fileUploadRepo.GetAll();



            //return View(sviFajlovi);
                       
        }


        

        public ActionResult Details(int id)
        {
            FileUploadModel fileUploadModel = new FileUploadModel();
            fileUploadModel = fileUploadRepo.GetById(id);
            return View(fileUploadModel);
        }

        [HttpGet]
        public ActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Create(HttpPostedFileBase fileUploaded, FileUploadModel fileUploadModel)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    /*&& fileUploadModel.Naziv!=null&&fileUploadModel.Opis!=null&&fileUploadModel.Cena<0*/

                    if (fileUploaded != null)
                    {
                        string fileName = Path.GetFileName(fileUploaded.FileName);
                        string extension = Path.GetExtension(fileUploaded.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        fileUploadModel.fileUploaded = "~/UploadedFiles/" + fileName;

                        fileName = Path.Combine(Server.MapPath("~/UploadedFiles"), fileName);

                        fileUploaded.SaveAs(fileName);

                        fileUploadRepo.Add(fileUploadModel);

                        ModelState.Clear();

                        ViewBag.ImageUrl = "UploadedFiles/" + fileName;



                    }
                    ViewBag.FileStatus = "File uploaded successfully.";
                }
                catch (Exception)
                {

                    ViewBag.FileStatus = "Error while file uploading.";
                }

            }
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            var fajlZaBrisanje = fileUploadRepo.GetById(id);
            if (fajlZaBrisanje != null)
            {
                fileUploadRepo.Delete(fajlZaBrisanje);

                return RedirectToAction("Index");
            }

            ViewBag.FileStatus = "Error while deleting file.";
            return RedirectToAction("Index");

        }
        [HttpGet]
        public ActionResult Edit(int id)
        {

            var data = fileUploadRepo.GetById(id);
            FileUploadModel fileUploadModel = new FileUploadModel
            {
                Id = data.Id,
                Naziv = data.Naziv,
                Opis = data.Opis,
                Cena = data.Cena,
                ZahtevanaKolicina = data.ZahtevanaKolicina,
                TrenutnaKolicina = data.TrenutnaKolicina,
                Razlika = data.Razlika,
                fileUploaded = data.fileUploaded
            };
            return View(fileUploadModel);
        }
        [HttpPost]
        public ActionResult Edit(int id,HttpPostedFileBase fileUploaded, FileUploadModel fileUploadModel)
        {
            
            FileUploadRepo fileUploadRepo = new FileUploadRepo();
            //if (ModelState.IsValid)
            //{
            //    var image = fileUploadRepo.GetById(id);
            //    //if (image == null)
            //    //{
            //    //    return new HttpNotFoundResult();
            //    //}

            //    image.Naziv = fileUploadModel.Naziv;
            //    image.Cena = fileUploadModel.Cena;
            //    image.Opis = fileUploadModel.Opis;
            //    image.ZahtevanaKolicina = fileUploadModel.ZahtevanaKolicina;
            //    image.TrenutnaKolicina = fileUploadModel.TrenutnaKolicina;
            //    image.Razlika = fileUploadModel.Razlika;
            //    image.fileUploaded = fileUploadModel.fileUploaded;
            //}
            ModelState.Clear();

            if (ModelState.IsValid)
            {
                try
                {
                    /*&& fileUploadModel.Naziv!=null&&fileUploadModel.Opis!=null&&fileUploadModel.Cena<0*/

                    if (fileUploaded != null)
                    {
                        string fileName = Path.GetFileName(fileUploaded.FileName);
                        string extension = Path.GetExtension(fileUploaded.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        fileUploadModel.fileUploaded = "~/UploadedFiles/" + fileName;

                        fileName = Path.Combine(Server.MapPath("~/UploadedFiles"), fileName);

                        fileUploaded.SaveAs(fileName);

                        //fileUploadRepo.Add(fileUploadModel);
                        fileUploadRepo.Update(fileUploadModel);



                        ModelState.Clear();

                        ViewBag.ImageUrl = "UploadedFiles/" + fileName;



                    }
                    ViewBag.FileStatus = "File uploaded successfully.";
                }
                catch (Exception)
                {

                    ViewBag.FileStatus = "Error while file uploading.";
                }

                //fileUploadRepo.Update(fileUploadModel);

                return RedirectToAction("Index");
            }

            return View(fileUploadModel);

        }
    }
}