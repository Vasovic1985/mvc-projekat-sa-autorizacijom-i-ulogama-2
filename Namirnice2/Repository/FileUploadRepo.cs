﻿using Namirnice2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Namirnice2.Repository
{
    public class FileUploadRepo:IDisposable, IUploadFile
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IQueryable<FileUploadModel> GetAll()
        {
            //return db.Proizvods.Include(x => x.Kategorija).OrderBy(x => x.Cena);

            return db.FileUploadModels;
        }

        public FileUploadModel GetById(int id)
        {
            FileUploadModel fileUploadModel = db.FileUploadModels.Find(id);
            return fileUploadModel;
        }

       

        public void Add(FileUploadModel fileUploadModel)
        {


            db.FileUploadModels.Add(fileUploadModel);
            db.SaveChanges();

        }

        public void Delete(FileUploadModel fileUploadModel)
        {
            db.FileUploadModels.Remove(fileUploadModel);
            db.SaveChanges();
        }

        public void Update(FileUploadModel fileUploadModel)
        {
          
            db.Entry(fileUploadModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

        }

        private bool FileUploadedExists(int id)
        {
            return db.FileUploadModels.Count(e => e.Id == id) > 0;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}